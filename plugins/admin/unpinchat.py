from main import api
from defadmin import chatAdmin
def Function(msg, cmd):
	if msg['chat']['type'] != 'private':
		if chatAdmin(msg["chat"]["id"], msg["from"]["id"]) != True: return "<b>You are not an administrator</b>"
		else:
			try: api.unpinChatMessage(msg['chat']['id'])
			except: return "<b>Try again</b>"
	else: 
		return "Hey I'm not a group"

plugin = {
	'patterns': [
	 "^[/!]((?:unpin)|(?:delpin))$"
],
	'function': Function,
	'name': "Unpinmessage",
	'admin': True,
	'usage': '<code>/unpin</code>: Remove pinned message',
	'sudo': False,
}